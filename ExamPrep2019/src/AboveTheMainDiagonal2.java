import java.io.BufferedReader;
import java.io.InputStreamReader;

public class AboveTheMainDiagonal2 {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(reader.readLine());

        long sum = 0;

        long matrix[][] = new long[n][n];

        for (int row = 0; row < n; row++) {
            for (int col = 0; col < n; col++) {
                matrix[row][col] = (long) Math.pow(2, row + col);
            }
        }
        for (int row = 0; row < n; row++) {
            for (int col = n - 1; col >= row; col--) {
                sum += matrix[row][col];
            }
        }
        System.out.println(sum);
    }
}