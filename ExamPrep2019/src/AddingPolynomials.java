import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class AddingPolynomials {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(reader.readLine());
        int[] polynomOne = Arrays.stream(reader.readLine().split(" "))
                .mapToInt(Integer::parseInt)
                .toArray();
        int[] polynomTwo = Arrays.stream(reader.readLine().split(" "))
                .mapToInt(Integer::parseInt)
                .toArray();

        StringBuilder result = new StringBuilder();

        for (int i = 0; i < n ; i++) {
            result.append(polynomOne[i] + polynomTwo[i] + " ");
        }
        System.out.println(result);
    }
}