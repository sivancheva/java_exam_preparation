import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

public class AlienMessage {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        List alphabet = Arrays.asList(new Character[]{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'});

        String englishWord = reader.readLine();
        String spanishWord = reader.readLine();

        StringBuilder result = new StringBuilder();

        int englishWordLength = englishWord.length();
        int spanishWordLength = spanishWord.length();

        if (englishWordLength <= spanishWordLength) {

            for (int i = 0; i < englishWordLength; i++) {

                char englischChar = englishWord.charAt(i);
                char spanishChar = spanishWord.charAt(i);

                if (englischChar == '-') {
                    result.append('-');
                    continue;
                }
                if (englischChar == ' ') {
                    result.append(' ');
                    continue;
                }
                if (spanishChar == '-') {
                    result.append('-');
                    continue;
                }
                if (spanishChar == ' ') {
                    result.append(' ');
                    continue;
                }

                int englishIndex = alphabet.indexOf(englischChar);
                int spanishIndex = alphabet.indexOf(spanishChar);

                int index = Math.abs(englishIndex - spanishIndex);
                result.append(alphabet.get(index));
            }

            String toAppend = spanishWord.substring(englishWordLength, spanishWordLength);
            result.append(toAppend);
        } else  {
            System.out.println();
        }

        System.out.println(result);
    }
}