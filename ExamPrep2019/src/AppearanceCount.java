import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class AppearanceCount {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(reader.readLine());
        String[] symbols = reader.readLine().split(" ");
        String searchedNumber = reader.readLine();

        int counter = 0;

        for (int i = 0; i < symbols.length -1 ; i++) {
            if (symbols[i].equals(searchedNumber)){
                counter++;
            }
        }
        System.out.println(counter);
    }
}