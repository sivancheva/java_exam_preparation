import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Arithmetics {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int a = Integer.parseInt(reader.readLine());
        int b = Integer.parseInt(reader.readLine());

        StringBuilder result = new StringBuilder();
        result.append(a+b + "\n");
        result.append(Math.abs(b-a) + "\n");
        result.append(a*b + "\n");
        result.append(a%b + "\n");
        result.append((int)Math.pow(a,b));
        System.out.println(result);
    }
}
