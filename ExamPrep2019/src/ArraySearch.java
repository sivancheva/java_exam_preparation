import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class ArraySearch {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String[] input = reader.readLine().split(",");

        StringBuilder result = new StringBuilder("");

        for (int i = 1; i <= input.length ; i++) {
            String searched = String.valueOf(i);
            boolean contains = Arrays.asList(input).contains(searched);
            if (!contains){
                result.append(searched + ",");
            }
        }

        if (result.length() > 1){
            result.deleteCharAt(result.length() - 1);
        }
        System.out.println(result);
    }
}
