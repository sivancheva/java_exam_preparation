import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ArraySortPreparation {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        List<String> newList = new ArrayList<>(Arrays.asList(reader.readLine().split(",")));
        int occurrencesOfZero = Collections.frequency(newList, "0");

        newList.removeAll(Collections.singleton("0"));

        for (int i = 0; i < occurrencesOfZero; i++) {
            newList.add("0");
        }

        System.out.println(String.join(",", newList));
    }
}