import java.io.BufferedReader;
import java.io.InputStreamReader;

public class BalancedNumbers {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int sum = 0;
        String input = reader.readLine();
        while (true){
            int first = input.charAt(0) - '0';
            int second = input.charAt(1) - '0';
            int third = input.charAt(2) - '0';

            if (first + third == second){
                sum += Integer.parseInt(input);

            }else {
                System.out.println(sum);
                break;
            }
            input = reader.readLine();
        }
    }
}