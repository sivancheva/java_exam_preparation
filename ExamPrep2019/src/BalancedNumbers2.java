import java.io.BufferedReader;
import java.io.InputStreamReader;

public class BalancedNumbers2 {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int sum = 0;

        while (true){
            String input = reader.readLine();
            int firstDigit = input.charAt(0) -'0';
            int secondDigit = input.charAt(1) -'0';
            int lastDigit = input.charAt(2) -'0';

            if(firstDigit + lastDigit == secondDigit){
                sum += Integer.parseInt(input);
            }else{
                break;
            }
        }

        System.out.println(sum);
    }
}