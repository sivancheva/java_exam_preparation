import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class BeginningAndEndings {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int[] intArray = Arrays.stream(reader.readLine().split(","))
                .mapToInt(Integer::parseInt)
                .toArray();

        int n = Integer.parseInt(reader.readLine());
        List<Integer> endings = new ArrayList<>();
        int rest = intArray.length   - n;

        for (int i = intArray.length -1; i >= rest; i--) {
            endings.add(intArray[i]);
        }

        Collections.reverse(endings);

        for (int i = 0; i < n; i++) {

            int firstNum = intArray[i];
            int secondNum = endings.get(i);
            if (firstNum != secondNum){
                System.out.printf("false %.0f,%.0f", (double)firstNum, (double)secondNum);
                return;
            }
        }

       StringBuilder result = new StringBuilder();
        result.append("true ");
        for (int num: endings){
            result.append(num +",");
        }
        result.deleteCharAt(result.lastIndexOf(","));

        System.out.println(result);

    }
}
