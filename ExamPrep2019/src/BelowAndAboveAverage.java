import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class BelowAndAboveAverage {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        List<String> list = new ArrayList<String>(Arrays.asList(reader.readLine().split(",")));

        List<Double> newList = list.stream()
                .map(s -> Double.parseDouble(s))
                .collect(Collectors.toList());

        Double average = newList.stream().mapToDouble(val -> val).average().orElse(0.0);
        StringBuilder belowAvr = new StringBuilder();
        StringBuilder aboveAvr = new StringBuilder();



        for (double d: newList){
            int dToAppend = (int)d;
            if (d > average){
                aboveAvr.append(dToAppend +",");
            }else if(d < average){
                belowAvr.append(dToAppend + ",");
            }
        }
        System.out.printf("avg: %.2f \n", average);

        belowAvr.deleteCharAt(belowAvr.lastIndexOf(","));
        aboveAvr.deleteCharAt(aboveAvr.lastIndexOf(","));

        System.out.println("below: "+belowAvr);
        System.out.println("above: "+aboveAvr);


    }
}
