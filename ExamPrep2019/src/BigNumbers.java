import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class BigNumbers {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int[] intArray = Arrays.stream(reader.readLine().split(" "))
                .mapToInt(Integer::parseInt)
                .toArray();
        int n = Math.max(intArray[0], intArray[1]);

        int[] firstArr = new int[n];
        int[] secondArr = new int[n];

        String[] input = reader.readLine().split(" ");

        for (int i = 0; i < intArray[0]; i++) {
            firstArr[i] = Integer.parseInt(input[i]);
        }
        String[] input2 = reader.readLine().split(" ");

        for (int i = 0; i < intArray[1]; i++) {
            secondArr[i] = Integer.parseInt(input2[i]);
        }

        StringBuilder result = new StringBuilder();

        int naum = 0;
        for (int i = 0; i < n; i++) {

            int currSum = firstArr[i] + secondArr[i];

            if (currSum + naum >= 10) {
                int toAppend = (currSum + naum) % 10;
                result.append(toAppend + " ");
                naum = 1;
            } else {
                result.append(currSum  + naum +" ");
                naum = 0;
            }
        }

        System.out.println(result);
    }
}