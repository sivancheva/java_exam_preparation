import java.io.BufferedReader;
import java.io.InputStreamReader;

public class BiggestOfThree {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        double max = -Double.MAX_VALUE;
        String result = "";
        for (int i = 0; i < 3; i++) {
            String input = reader.readLine();
            Double currNum = Double.parseDouble(input);
            if (currNum > max){
                max = currNum;
                result = input;
            }
        }

        System.out.printf(result);
    }
}