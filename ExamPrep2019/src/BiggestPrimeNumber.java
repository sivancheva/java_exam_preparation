import java.io.BufferedReader;
import java.io.InputStreamReader;

public class BiggestPrimeNumber {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int num = Integer.parseInt(reader.readLine());
        int maxPrime = 0;
        for (int i = num; i >= 2 ; i--) {
           boolean checkIfPrime = IsPrime(i);
           if (checkIfPrime){
               maxPrime = i;
               break;
           }
        }
        System.out.println(maxPrime);
    }

    private static boolean IsPrime (int num){
        boolean isPrime = true;
        for(int divisor = 2; divisor <= num / 2; divisor++) {
            if (num % divisor == 0) {
                isPrime = false;
                break; // num is not a prime, no reason to continue checking
            }
        }
        return isPrime;
    }
}