import java.io.BufferedReader;
import java.io.InputStreamReader;

public class BinaryDigitsCount {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int digit = Integer.parseInt(reader.readLine());
        int n = Integer.parseInt(reader.readLine());
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < n; i++) {
            Long numToCheck = Long.parseLong(reader.readLine());
            int count = CountElements(numToCheck, digit);
            result.append(count + "\n");
        }
        System.out.println(result);
    }

    private static int CountElements(long num, int digit) {

        long numToConvert = num;
        int count = 0;

        if (numToConvert == 0) {
            if (digit == 0) {
                count++;
            }
        }
        while (numToConvert > 0) {
            long index = numToConvert % 2;
            if (index == digit) {
                count++;
            }
            numToConvert /= 2;
        }
        return count;
    }
}