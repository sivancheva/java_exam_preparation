import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class BinaryToDecimal {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int[] intArray = Arrays.stream(reader.readLine().split(""))
                .mapToInt(Integer::parseInt)
                .toArray();

        int sum = 0;
        int pow = 0;

        for (int i = intArray.length -1 ; i >= 0 ; i--) {
            sum += intArray[i]*(Math.pow(2,pow));
            pow++;
        }
        System.out.println(sum);
    }
}
