import java.util.Scanner;

public class BonusScore {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int num = in.nextInt();


        if (num >= 1 && num <= 3) {
            num *= 10;
        }else if (num >= 4 && num <= 6){
            num *= 100;
        }else if (num >= 7 && num <= 9){
            num *= 1000;
        }else{
            System.out.println("invalid score");
            return;
        }
        System.out.println(num);
    }
}

