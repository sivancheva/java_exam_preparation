import java.io.BufferedReader;
import java.io.InputStreamReader;

public class BottleDeposit {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        double sum = 0;

        sum += Double.parseDouble(reader.readLine()) * 0.1;
        sum += Double.parseDouble(reader.readLine()) * 0.25;

        System.out.printf("%.2f", sum);
    }
}
