import java.util.Scanner;

public class Calculate {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        double n = in.nextDouble();
        double x = in.nextDouble();

        double sum = 1;

        for (int i = 1; i <= n ; i++) {

            double delimo = 1;
            for (int j = 1; j <= i ; j++) {
                delimo *= j;
            }
            double delitel = Math.pow(x, i);

            sum += delimo/delitel;
        }

        System.out.printf("%.5f",sum);
    }
}
