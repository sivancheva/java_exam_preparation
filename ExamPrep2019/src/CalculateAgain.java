import java.math.BigInteger;
import java.util.Scanner;

public class CalculateAgain {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int n = in.nextInt();
        int k = in.nextInt();

        BigInteger result = new BigInteger("1");
        for (long i = k + 1; i <= n ; i++) {

            result = result.multiply(BigInteger.valueOf(i));
        }

        System.out.println(result);
    }
}

