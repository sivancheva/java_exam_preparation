import java.io.BufferedReader;
import java.io.InputStreamReader;

public class CalculateChangePreparation {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        double price = Double.parseDouble(reader.readLine());
        double paid = Double.parseDouble(reader.readLine());

        double difference = paid - price;
        if (difference >= 1){
            System.out.printf("%d x 1 lev \n", (int)difference);
        }

        double currAmount = (difference - (int)difference)*100;
        double tmp;
        if(currAmount >= 50)
        {
            tmp = (int) currAmount/50;
            System.out.println ((int)tmp +" x 50 stotinki");
            currAmount = currAmount % 50;
        }
        if(currAmount >= 20)
        {
            tmp = (int) currAmount/20;
            System.out.println ((int)tmp +" x 20 stotinki");
            currAmount = currAmount % 20;
        }
        if(currAmount >= 10)
        {
            tmp = (int) currAmount/10;
            System.out.println ((int)tmp +" x 10 stotinki");
            currAmount = currAmount % 10;
        }
        if(currAmount >= 5)
        {
            tmp = (int) currAmount/5;
            System.out.println ((int)tmp +" x 5 stotinki");
            currAmount = currAmount % 5;
        }
        if(currAmount >= 2)
        {
            tmp = (int) currAmount/2;
            System.out.println ((int)tmp +" x 2 stotinki");
            currAmount = currAmount % 2;
        }
        if(currAmount >= 1)
        {
            tmp = (int) currAmount/1;
            System.out.println ((int)tmp +" x 1 stotinka");
            currAmount = currAmount % 1;
        }

//        System.out.println();
    }
}