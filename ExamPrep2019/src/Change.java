import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigInteger;

public class Change {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        BigInteger result = new BigInteger("0");
        String[] input = reader.readLine().split(" ");

        String cash = input[0];
        String priceAsStr = input[1];
        BigInteger cashLoni = new BigInteger(cash);
        BigInteger price = (new BigInteger(priceAsStr));

        result = cashLoni.subtract(price);
        System.out.println(result);
    }
}