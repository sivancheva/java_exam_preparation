import java.util.Arrays;
import java.util.Scanner;

public class CheckForPlayCard {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        String card = in.nextLine();

        String[] cards = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"};

        boolean contains = Arrays.stream(cards).anyMatch(card::equals);
        if (contains){
            System.out.printf("yes %s", card);
        }else{
            System.out.printf("no %s", card);
        }
    }
}
