import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class ChessSquareColor {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int c = reader.readLine().charAt(0);
        int r = Integer.parseInt(reader.readLine());

        Boolean isDark = (c % 2) == (r % 2);

        if (isDark){
            System.out.println("dark");

        }else{
            System.out.println("light");
        }
    }
}