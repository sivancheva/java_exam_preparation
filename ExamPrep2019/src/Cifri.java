import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Cifri {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int[] intArray = Arrays.stream(reader.readLine().split(" "))
                .mapToInt(Integer::parseInt)
                .toArray();
        int a = intArray[0];
        int b = intArray[1];
        int p1 = intArray[2];
        int p2 = intArray[3];


        String digits = "";

        for (int i = a; i <= b ; i++) {
            digits += i;
        }

        int countNotDividable = 0;

        for (int i = 0; i < digits.length(); i++) {
            int digit = digits.charAt(i) -'0';

            if (digit% p1 != 0 && digit%p2 != 0 ){
                countNotDividable++;
            }
        }
        System.out.println(countNotDividable);

    }


}