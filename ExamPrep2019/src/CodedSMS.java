import java.io.BufferedReader;
import java.io.InputStreamReader;

public class CodedSMS {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String input = reader.readLine();
        char currChar = input.charAt(0);

        StringBuilder wholeWord = new StringBuilder().append(currChar);

        for (int i = 1; i < input.length(); i++) {
            currChar = input.charAt(i);

            StringBuilder toAppend = new StringBuilder(wholeWord);
            wholeWord.append(currChar);
            wholeWord.append(toAppend);
        }

        System.out.println(wholeWord);
    }
}
