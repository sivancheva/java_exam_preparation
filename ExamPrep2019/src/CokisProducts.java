import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class CokisProducts {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Map<String, Double> priceList = new HashMap<>();

        int n = Integer.parseInt(reader.readLine());

        for (int i = 0; i < n ; i++) {
            String[] input = reader.readLine().split(" ");
            input = Arrays.stream(input).filter(s -> !s.isEmpty()).toArray(String[]::new);
            String product = "";
            Double price = 0.0;
            if (input.length == 2){
                product = input[0];
                price = Double.parseDouble(input[1]);
            }else{
                for (int j = 0; j < input.length-1 ; j++) {
                    product += input[j] + " ";
                }
                price = Double.parseDouble(input[input.length-1]);
            }
            priceList.put(product.trim(), price);
        }

        int m = Integer.parseInt(reader.readLine());

        for (int i = 0; i < m; i++) {
            double totalSum = 0.0;
            String[] list = reader.readLine().split(",");

            list = Arrays.stream(list).filter(s -> !s.isEmpty()).toArray(String[]::new);
            for (int j = 0; j < list.length; j++) {
                String[] currProductInformation = list[j].split(" ");
                currProductInformation = Arrays.stream(currProductInformation).filter(s -> !s.isEmpty()).toArray(String[]::new);
                int lengthOfCurrProductInfo = currProductInformation.length;

                double quantity = 1;
                String product = "";
                if (lengthOfCurrProductInfo == 2){
                    if (Character.isDigit(currProductInformation[0].charAt(0))){
                        quantity = Double.parseDouble(currProductInformation[0]);
                        product = currProductInformation[1].trim();
                    }else{
                        product = ReturnProduct(currProductInformation);
                    }
                }else if(lengthOfCurrProductInfo == 1){
                    product = currProductInformation[0];
                }else{
                    quantity = Double.parseDouble(currProductInformation[0]);
                    String[] productName = new String[lengthOfCurrProductInfo-1];
                    System.arraycopy( currProductInformation, 1, productName, 0, lengthOfCurrProductInfo-1 );
                    product = ReturnProduct(productName);
                }
                if (priceList.containsKey(product.trim())){
                    totalSum += quantity*priceList.get(product.trim());
                }
            }
            System.out.printf("%.2f \n",totalSum);
        }
    }

    private static String ReturnProduct (String[] products){
        String product = "";
        for (int k = 0; k < products.length ; k++) {
            product += products[k].trim() + " ";
        }
        return product.trim();
    }
}

