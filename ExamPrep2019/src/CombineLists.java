import java.io.BufferedReader;
import java.io.InputStreamReader;

public class CombineLists {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String[] input1 = reader.readLine().split(",");
        String[] input2 = reader.readLine().split(",");

        StringBuilder result = new StringBuilder();
        for (int i = 0; i < input1.length ; i++) {
            result.append(input1[i] + ",");
            result.append(input2[i] + ",");
        }
        result.deleteCharAt(result.lastIndexOf(","));

        System.out.println(result);
    }
}