import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ConvertDegrees {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String[] input = reader.readLine().split(" ");

        for (String num:input){
            double celsius = Double.parseDouble(num);
            int fahrenheit = (int)Math.round(celsius*9/5 + 32);
            System.out.println(fahrenheit);
        }
    }
}
