import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Converter {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int[] intArray = Arrays.stream(reader.readLine().split(" "))
                .mapToInt(Integer::parseInt)
                .toArray();

        System.out.println();
    }
}
