import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigInteger;

public class Counting {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        BigInteger bi = new BigInteger(reader.readLine());

        for (int i = 1; i <= 10 ; i++) {
           bi = bi.add(BigInteger.valueOf(1));
            System.out.println(bi);
        }
    }
}
