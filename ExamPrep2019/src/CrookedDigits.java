import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class CrookedDigits {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String input = reader.readLine();
        input = input.replace("-","");
        input = input.replace(".","");

        String num = input;

        while (true){
            if (num.length() == 1){
                break;
            }
            int sum = 0;
            for (int i = 0; i < num.length() ; i++) {
                sum += num.charAt(i) - '0';
            }
            num = Integer.toString(sum);
        }
        System.out.println(num);
    }
}