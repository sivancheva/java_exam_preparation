import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigInteger;

public class CrookedDigits2 {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String input = reader.readLine();

        input = input.replace("-", "");
        input = input.replace(".", "");

        BigInteger sum = new BigInteger(input);
        BigInteger comparar = new BigInteger("9");

        while (true) {
            if (sum.compareTo(comparar) == -1 || sum.compareTo(comparar) == 0) {
                break;
            }

            BigInteger currSum = new BigInteger("0");
            String biToString = sum.toString();

            for (int i = 0; i < biToString.length(); i++) {

                BigInteger biToAdd = new BigInteger(String.valueOf(biToString.charAt(i)));
                currSum = currSum.add(biToAdd);
            }
            sum = currSum;
        }

        System.out.println(sum);
    }
}