import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Crossword {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int[] intArray = Arrays.stream(reader.readLine().split(" "))
                .mapToInt(Integer::parseInt)
                .toArray();
        int rows = intArray[0];
        int cols = intArray[1];

        String[][] matrix = new String[rows][cols];
        List<String> words = new ArrayList<String>();

        for (int r = 0; r < rows; r++) {
            String line = reader.readLine();

            // extract horizontal words
            String[] line2 = line.split("#");
            List<String> horizontalWords = Arrays.asList(line2);
            horizontalWords = horizontalWords.stream().filter(s -> s.length() > 1).collect(Collectors.toList());
            words.addAll(horizontalWords);

            String[] input1 = line.split("");
            for (int c = 0; c < cols ; c++) {
                matrix[r][c] = input1[c];
            }
        }

        String[][] rotadedMatrix = new String[cols][rows];

        for (int r = 0; r < cols; r++) {
            for (int c = 0; c < rows ; c++) {
                rotadedMatrix[r][c] = matrix[c][r];
            }
        }

        List<String> vert = new ArrayList<String>();

        for (int i = 0; i < cols ; i++) {
            String currStr = "";
            for (int j = 0; j < rows; j++) {
                    currStr += rotadedMatrix[i][j];
            }
            String[] ver = currStr.split("#");
            List<String> v = Arrays.asList(ver);
            vert.addAll(v);
        }

        vert = vert.stream().filter(s -> s.length() > 1).collect(Collectors.toList());
        words.addAll(vert);
        Collections.sort(words);

        System.out.println(words.get(0));
    }

}

