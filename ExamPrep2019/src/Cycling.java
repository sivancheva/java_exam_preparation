import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Cycling {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(reader.readLine().trim());
        int day = 0;
        int timeTravelledInMinutes = 0;
        double maxSPeed = 0;

        for (int i = 1; i <= n; i++) {
            int[] intArray = Arrays.stream(reader.readLine().split(" "))
                    .mapToInt(Integer::parseInt)
                    .toArray();
            int minutesTotal = intArray[1] + intArray[0]*60;
            timeTravelledInMinutes += minutesTotal;
            int mTraveled = intArray[2];

            double speed = (double) mTraveled/minutesTotal;
            if (speed > maxSPeed){
                day = i;
                maxSPeed = speed;
            }
        }

        int days = timeTravelledInMinutes / 24 / 60;
        int hours = timeTravelledInMinutes / 60 % 24;
        int minutes = timeTravelledInMinutes % 60;

        System.out.printf("%d %d %d %d", days, hours, minutes, day);

    }
  }