import java.io.BufferedReader;
import java.io.InputStreamReader;

public class DecimalToBinary {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int num = Integer.parseInt(reader.readLine());

        StringBuilder result = new StringBuilder();

        if (num == 0){
            System.out.println(num);
            return;
        }
        while (num > 0){
            int index = num%2;
            result.insert(0, index);
            num /= 2;
        }
        System.out.println(result);
    }
}


