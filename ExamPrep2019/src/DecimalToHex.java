import java.io.BufferedReader;
import java.io.InputStreamReader;

public class DecimalToHex {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        long num = Long.parseLong(reader.readLine());

        String[] reminders = {"0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F","10"};

        StringBuilder result = new StringBuilder();

        if (num == 0){
            System.out.println(0);
            return;
        }
        while (num > 0){

            long index = num%(long)16;
            result.insert(0,reminders[(int)index]);
            num /= 16;
        }
        System.out.println(result);
    }
}