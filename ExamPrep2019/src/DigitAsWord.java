import java.io.BufferedReader;
import java.io.InputStreamReader;

public class DigitAsWord {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String[] digits = {"zero","one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};

        String input = reader.readLine();

        try {
            System.out.println(digits[Integer.parseInt(input)]);
        }catch (Exception e){
            System.out.println("not a digit");
        }
    }
}