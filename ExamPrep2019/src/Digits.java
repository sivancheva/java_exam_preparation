import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Digits {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String input = reader.readLine();

        input = input.replaceAll("\\D+","");

        int sum = 0;

        if (input.length()>0){
            for (int i = 0; i < input.length() ; i++) {

                sum += input.charAt(i) -'0';
            }
        }else{
            sum = -1;
        }
        System.out.println(sum);
    }
}