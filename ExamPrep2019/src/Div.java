import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.util.Arrays;

public class Div {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int[] intArray = Arrays.stream(reader.readLine().split(" "))
                .mapToInt(Integer::parseInt)
                .toArray();
        int a = intArray[0];
        int b = intArray[1];

        int gcd = gcdThing(a,b);

        StringBuilder result = new StringBuilder();
        boolean foundPrime = false;
        for (int i = 2; i <= gcd ; i++) {
            if (gcd%i == 0 && IsPrime(i)){
                result.append(i + " ");
                foundPrime = true;
            }
        }
        if (foundPrime){
            System.out.println(result);
        }else{
            System.out.println("-1");
        }
    }
    private static int gcdThing(int a, int b) {
        BigInteger b1 = BigInteger.valueOf(a);
        BigInteger b2 = BigInteger.valueOf(b);
        BigInteger gcd = b1.gcd(b2);
        return gcd.intValue();
    }
    private static boolean IsPrime (int num){
        boolean isPrime = true;
        for(int divisor = 2; divisor <= num / 2; divisor++) {
            if (num % divisor == 0) {
                isPrime = false;
                break; // num is not a prime, no reason to continue checking
            }
        }
        return isPrime;
    }
}
