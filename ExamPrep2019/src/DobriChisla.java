import java.util.Scanner;

public class DobriChisla {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        String[] input = in.nextLine().split(" ");

        int startNum = Integer.parseInt(input[0]);
        int endNum = Integer.parseInt(input[1]);

        int countGoodNums = 0;

        for (int i = startNum; i <= endNum ; i++) {
            boolean isGood = true;

            int currNum = i;

            while(currNum > 0 ){
                int currDigit = currNum % 10;

                if (currDigit != 0 && i % currDigit != 0){
                    isGood = false;
                    break;
                }
                currNum = currNum/10;
            }
            if (isGood){
                countGoodNums++;
            }
        }
        System.out.println(countGoodNums);
    }
}
