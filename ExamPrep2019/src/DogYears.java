import java.io.BufferedReader;
import java.io.InputStreamReader;

public class DogYears {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int humanYears = Integer.parseInt(reader.readLine());
        int dogYears = 0;

        for (int i = 1; i <= humanYears ; i++) {
            if (i <= 2 ){
                dogYears += 10;
            }else{
                dogYears += 4;
            }
        }

        System.out.println(dogYears);
    }
}