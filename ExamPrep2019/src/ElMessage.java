import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

//NOT Soleved

public class ElMessage {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String[] line = reader.readLine().split("\\.");
        String input = line[0];

        String[] splitted = input.split(" ");

        int maxCount = 0;

        for (String word: splitted){
            String ops[] = word.split("[a-zA-Z0-9]+");
            ops = Arrays.stream(ops).filter(s -> !s.isEmpty()).toArray(String[]::new);

            for (int i = 0; i < ops.length ; i++) {
                int currCount = 1;
                for (int j = 0; j < ops[i].length()-1 ; j++) {
                    String currString = ops[i];
                    char first = currString.charAt(j);
                    char second = currString.charAt(j+1);

                    if (first == second){
                        currCount++;
                    }else{
                        currCount = 1;
                    }
                }
                if (currCount > maxCount){
                    maxCount = currCount;
                }
            }
        }

        System.out.println(maxCount);
    }
}

