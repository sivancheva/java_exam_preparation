import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Elmessage2 {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String input = reader.readLine().split("\\.")[0];
        String[] inputArr = input.split(" ");

        char c = inputArr[0].charAt(0);

        boolean check = isLetterOrDigit(c);
        System.out.println();
    }

    private static boolean isLetterOrDigit (char c){

        return  (c >= 48 && c <= 57 || c >= 65 && c <= 90 || c>= 97 && c <= 122 || c == 32);

    }
}
