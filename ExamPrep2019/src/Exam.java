import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Exam {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));


        int[] input = Arrays.stream(reader.readLine().split(" "))
                .mapToInt(Integer::parseInt)
                .toArray();
        int studentsCount = input[0];
        int bezHimikalCount = input[1];
        int sHimikal = input[2];

        int[] students = new int[studentsCount];

        for (int i = 0; i < studentsCount; i++) {
            students[i] = 1;
        }
        int[] bezHimikal = Arrays.stream(reader.readLine().split(" "))
                .mapToInt(Integer::parseInt)
                .toArray();

        for (int i = 0; i < bezHimikal.length; i++) {
            int currStudentBezHimikal = bezHimikal[i];
            students[currStudentBezHimikal-1] = 0;
        }

        int[] susHimikal = Arrays.stream(reader.readLine().split(" "))
                .mapToInt(Integer::parseInt)
                .toArray();

        for (int i = 0; i < susHimikal.length; i++) {
            int currStudentSusHimikal = susHimikal[i];
            students[currStudentSusHimikal-1] = 2;
        }

        for (int i = 0; i < students.length; i++) {
            if (students[i] == 0){
                try {
                    if (students[i-1] == 2){
                        students[i-1] = 1;
                        students[i] = 1;
                        bezHimikalCount--;
                    }else if (students[i+1] == 2){
                        students[i] = 1;
                        students[i+1] = 1;
                        bezHimikalCount--;
                    }
                }catch (Exception e){
                    continue;
                }
            }
        }
        System.out.println(bezHimikalCount);
    }
}