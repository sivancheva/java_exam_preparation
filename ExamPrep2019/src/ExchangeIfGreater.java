import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.DecimalFormat;

public class ExchangeIfGreater {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        Double firstNum = Double.parseDouble(reader.readLine());
        Double secondNum = Double.parseDouble(reader.readLine());

        DecimalFormat df = new java.text.DecimalFormat("#.######");

        String firstNumFormated = df.format(firstNum);
        String secondNumFormated = df.format(secondNum);

        StringBuilder result = new StringBuilder();
        if (firstNum < secondNum) {
            result.append(firstNumFormated + " ");
            result.append(secondNumFormated);
        } else {
            result.append(secondNumFormated + " ");
            result.append(firstNumFormated);
        }

        System.out.println(result);
    }
}