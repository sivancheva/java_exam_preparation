import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Excursion {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int[] busSize = Arrays.stream(reader.readLine().split(" "))
                .mapToInt(Integer::parseInt)
                .toArray();
        int busWidth = busSize[0];
        int busHeigh = busSize[1];

        int broiPrepjatstvija = Integer.parseInt(reader.readLine().trim());
        int nomerNaPrepjatstvie = 0;

        for (int i = 1; i <= broiPrepjatstvija; i++) {
            String[] currPrepjatsvie = reader.readLine().split(" ");
            int currWidth = Integer.parseInt(currPrepjatsvie[0]);
            int currHeigh = Integer.parseInt(currPrepjatsvie[1]);

            if (busWidth <= currWidth && busHeigh <= currHeigh){
                continue;
            }else{
                nomerNaPrepjatstvie = i;
                break;
            }
        }
        if (nomerNaPrepjatstvie == 0){
            System.out.println("No crash");
        }else{
            System.out.println(nomerNaPrepjatstvie);
        }
    }
}