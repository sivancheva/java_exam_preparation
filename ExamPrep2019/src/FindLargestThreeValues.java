import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FindLargestThreeValues {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(reader.readLine());

        List<Integer> result = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            result.add(Integer.parseInt(reader.readLine()));
        }

        Collections.sort(result);

        int size = result.size();
        int max = result.get(size-1);
        int middle = result.get(size-2);
        int last = result.get(size-3);

        System.out.printf("%d, %d and %d", max, middle, last);
    }
}