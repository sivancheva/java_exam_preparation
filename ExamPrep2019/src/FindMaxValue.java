import java.io.BufferedReader;
import java.io.InputStreamReader;

public class FindMaxValue {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(reader.readLine());

        int max = Integer.MIN_VALUE;

        for (int i = 0; i < n; i++) {
            int number = Integer.parseInt(reader.readLine());
            if (number > max){
                max = number;
            }
        }
        System.out.println(max);
    }
}