import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class FirstLargestThanNeighbours {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(reader.readLine());
        int[] intArray = Arrays.stream(reader.readLine().split(" "))
                .mapToInt(Integer::parseInt)
                .toArray();

        int max = -1;

        for (int i = 0; i < intArray.length-1; i++) {
            if (intArray[i] > intArray[i+1]){

                System.out.println(i);
                return;
            }
        }
        System.out.println(max);
    }
}