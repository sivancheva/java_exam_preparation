import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ForestRoad {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(reader.readLine());
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < n; i++) {
            StringBuilder toAppend = new StringBuilder();
            for (int j = 0; j < n; j++) {
                toAppend.append(".");
            }
            toAppend.setCharAt(i, '*');
            toAppend.append("\n");
            result.append(toAppend);
        }

        for (int i = n-2; i >=0; i--) {
            StringBuilder toAppend = new StringBuilder();
            for (int j = n-1; j >= 0 ; j--) {
                toAppend.append(".");
            }
            toAppend.setCharAt(i, '*');
            toAppend.append("\n");
            result.append(toAppend);
        }
        System.out.println(result);
    }
}