import java.io.BufferedReader;
        import java.io.InputStreamReader;
        import java.math.BigInteger;

public class Frac {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String[] input = reader.readLine().split(" ");
        int chislitel = Integer.parseInt(input[0]);
        int znamenatel = Integer.parseInt(input[1]);

        int gcd = gcdThing(chislitel, znamenatel);

        System.out.printf("%d %d",chislitel/gcd, znamenatel/gcd);
    }
    private static int gcdThing(int a, int b) {
        BigInteger b1 = BigInteger.valueOf(a);
        BigInteger b2 = BigInteger.valueOf(b);
        BigInteger gcd = b1.gcd(b2);
        return gcd.intValue();
    }
}
