import java.util.Scanner;

public class Game {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        String input = in.nextLine();

        int[] numbers = new int[input.length()];

        for (int i = 0; i < input.length(); i++) {
            numbers[i] = input.charAt(i) - '0';
        }
        int firstNum = numbers[0];
        int secondNum = numbers[1];
        int thirdNum = numbers[2];

        int maxSum = Integer.MIN_VALUE;

        if(firstNum + secondNum +thirdNum > maxSum){
            maxSum = firstNum + secondNum +thirdNum;
        }
        if (firstNum*secondNum*thirdNum > maxSum){
            maxSum = firstNum*secondNum*thirdNum;
        }
        if((firstNum + secondNum*thirdNum) > maxSum){
            maxSum = firstNum + secondNum*thirdNum;
        }
        if (firstNum*secondNum + thirdNum > maxSum){
            maxSum = firstNum*secondNum + thirdNum;
        }
        System.out.println(maxSum);
    }
}
