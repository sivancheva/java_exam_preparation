import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class GetLargestNumber {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int[] intArray = Arrays.stream(reader.readLine().split(" "))
                .mapToInt(Integer::parseInt)
                .toArray();
        int firstNum = intArray[0];
        int secondNum = intArray[1];
        int thirdNum = intArray[2];

        int result = GetMax(firstNum, secondNum, thirdNum);
        System.out.println(result);
    }

    public static int GetMax(int a, int b, int c){
        int max = Integer.MIN_VALUE;

        if (a > max){
            max = a;
        }
        if (b > max){
            max = b;
        }
        if (c > max){
            max = c;
        }
        return max;
    }

}