import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

//lokalno testut izliza v judje izliza i 0 otpred i otzad

public class GolemiChisla {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(reader.readLine().trim());
        String[] read = reader.readLine().split(" ");

        String input = "";
        for (int i = 0; i < n; i++) {
            input += read[i];
        }

        Set<String> out = new HashSet<>();
        out = findAllPalindromesUsingBruteForceApproach(input);

        int maxLength = 0;
        String output = "";

        for (String s: out){
            if (s.length() > maxLength){
                maxLength = s.length();
                output = s;
            }
        }
        System.out.println(output);
    }

   private static Set<String> findAllPalindromesUsingBruteForceApproach(String input) {
        Set<String> palindromes = new HashSet<>();
        for (int i = 0; i < input.length(); i++) {
            for (int j = i + 1; j <= input.length(); j++) {
                if (isPalindrome(input.substring(i, j))) {
                    palindromes.add(input.substring(i, j));
                }
            }
        }
        return palindromes;
    }

    private static boolean isPalindrome(String input) {
        StringBuilder plain = new StringBuilder(input);
        StringBuilder reverse = plain.reverse();
        return (reverse.toString()).equals(input);
    }
}