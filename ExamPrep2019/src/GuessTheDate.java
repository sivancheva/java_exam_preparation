import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class GuessTheDate {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String year = reader.readLine();
        String month = reader.readLine();
        String day = reader.readLine();


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String strDate = year + "-" + month + "-" + day;
        Date date = sdf.parse(strDate);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, -1);
        Date yesterday = calendar.getTime();

        if (Integer.parseInt(day) >= 2 && Integer.parseInt(day) <= 10){
            System.out.println(new SimpleDateFormat("d-MMM-yyyy").format(yesterday));
        }else{
            System.out.println(new SimpleDateFormat("dd-MMM-yyyy").format(yesterday));
        }
    }
}