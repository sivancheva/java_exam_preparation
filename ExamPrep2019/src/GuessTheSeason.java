import java.io.BufferedReader;
import java.io.InputStreamReader;

public class GuessTheSeason {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String month = reader.readLine();
        int day = Integer.parseInt(reader.readLine());

        if ((month.equals("March") && day >= 20) || (month.equals("April") || month.equals("May") ) || (month.equals("June") && day <= 20)){
            System.out.println("Spring");
        }else if((month.equals("June") && day >= 21) || (month.equals("July") || month.equals("August") ) || (month.equals("September") && day <= 21)){
            System.out.println("Summer");
        }else if((month.equals("September") && day >= 22) || (month.equals("October") || month.equals("November") ) || (month.equals("December") && day <= 20)){
            System.out.println("Autumn");
        }else{
            System.out.println("Winter");
        }

        System.out.println();
    }
}