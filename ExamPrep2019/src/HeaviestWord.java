import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

public class HeaviestWord {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(reader.readLine());

        List alphabet = Arrays.asList(new Character[] {' ', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'});

        int maxSum = 0;
        String heaviestWord = "";

        for (int i = 0; i < n; i++) {

            String wordOriginal = reader.readLine();
            String word = wordOriginal.toLowerCase();
            int currSum = 0;

            for (int j = 0; j < word.length(); j++) {
                char c = word.charAt(j);
                currSum += alphabet.indexOf(c);
            }
            if (currSum > maxSum){
                maxSum = currSum;
                 heaviestWord = wordOriginal;
            }
        }

        System.out.println(maxSum + " " + heaviestWord);
    }
}