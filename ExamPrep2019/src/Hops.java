import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

//TLE 11/20 - correct, others > 0.3s
public class Hops {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int[] carrots = Arrays.stream(reader.readLine().split(", "))
                .mapToInt(Integer::parseInt)
                .toArray();
        int n = Integer.parseInt(reader.readLine().trim());

        int maxSum = Integer.MIN_VALUE;

        for (int i = 0; i < n; i++) {
            int[] carrotsCopy = Arrays.copyOf(carrots, carrots.length);
            int[] hops = Arrays.stream(reader.readLine().split(", "))
                    .mapToInt(Integer::parseInt)
                    .toArray();
            int sum = carrotsCopy[0];
            carrotsCopy[0] = 0;
            int startPos = 0;

            for (int j = 0; j < hops.length; j++) {
                int jumps = hops[j];
                int nextPosition = 0;
                if (jumps > 0){
                    nextPosition = startPos + jumps;
                }else if(jumps < 0){
                    nextPosition = startPos - Math.abs(jumps);
                }
                if (isInCarrotsField(carrotsCopy, nextPosition)){
                    sum += carrotsCopy[nextPosition];
                    carrotsCopy[nextPosition] = 0;
                    startPos = nextPosition;
                }else {
                    break;
                }
            }

            if (sum > maxSum){
                maxSum = sum;
            }
        }
        System.out.println(maxSum);
    }

    private  static  boolean isInCarrotsField (int[] carrotsField, int position){
        if (position >= carrotsField.length || position < 0 || carrotsField[position] == 0){
            return  false;
        }
        return true;
    }
}