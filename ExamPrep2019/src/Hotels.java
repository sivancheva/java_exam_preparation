import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Hotels {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(reader.readLine().trim());
        int[] hotelsArr = Arrays.stream(reader.readLine().split(" "))
                .mapToInt(Integer::parseInt)
                .toArray();

        int leftCount = 1;
        int rightCount = 1;

        int highestHotel = hotelsArr[0];
        for (int i = 0; i < n; i++) {
            ;
            if (hotelsArr[i] > highestHotel) {
                leftCount++;
                highestHotel = hotelsArr[i];
            }
        }

        highestHotel = hotelsArr[n - 1];
        for (int i = n - 1; i >= 0; i--) {
            if (hotelsArr[i] > highestHotel) {
                rightCount++;
                highestHotel = hotelsArr[i];
            }
        }
        System.out.printf("%d %d", leftCount, rightCount);
    }
}