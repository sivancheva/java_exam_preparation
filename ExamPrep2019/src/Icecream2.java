import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Icecream2 {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String[] input = reader.readLine().split(" ");

        int countElephants = Integer.parseInt(input[0]);
        String number = input[1];

        int numberLength = number.length();
        int elephantsWithowtIcecream = 0;

        if (numberLength <= countElephants){
            elephantsWithowtIcecream = ElephantenWithoutIcecreamNumLengthSmallerThanElephantencount(number, countElephants);
        }else if (numberLength > countElephants){
            String ops[] = number.split("");
            ops = Arrays.stream(ops).filter(s -> !s.equals("0")).toArray(String[]::new);

            int minSize = Math.min(ops.length, countElephants);

            if (minSize <= countElephants){
                elephantsWithowtIcecream = ElephantenWithoutIcecreamNumLengthSmallerThanElephantencount(number, countElephants);
            }else{
                for (int i = 0; i < minSize; i++) {
                    elephantsWithowtIcecream = countElephants-ops.length;
                }
            }
        }

        System.out.println(elephantsWithowtIcecream);
    }

    private static int ElephantenWithoutIcecreamNumLengthSmallerThanElephantencount (String num, int countElephanten){
        int elephantenWithoutIcevream = 0;
        elephantenWithoutIcevream = countElephanten - num.length();
        for (int i = 0; i < num.length(); i++) {
            int currDigit = num.charAt(i)- '0';
            if (currDigit == 0){
                elephantenWithoutIcevream++;
            }
        }
        return  elephantenWithoutIcevream;
    }
}