import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.util.Arrays;

public class IgraSChisla {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int[] intArray = Arrays.stream(reader.readLine().split(" "))
                .mapToInt(Integer::parseInt)
                .toArray();

        int countElephants = intArray[0];
        int lonysNumber = intArray[1];

        int[] elephantsNumbers = Arrays.stream(reader.readLine().split(" "))
                .mapToInt(Integer::parseInt)
                .toArray();

        StringBuilder result = new StringBuilder();
        int maxDeliteli = 0;

        for (int i = 0; i < elephantsNumbers.length; i++) {

            int currentElephantenNumber = elephantsNumbers[i];

            int gcd = gcdThing(lonysNumber, currentElephantenNumber);
            int countDeliteli = 0;

            for (int j = gcd; j > 1; j--) {
                if (lonysNumber % j == 0 && currentElephantenNumber % j == 0) {
                    countDeliteli++;
                }
            }

            if (countDeliteli > maxDeliteli) {
                maxDeliteli = countDeliteli;
                result = new StringBuilder();
                result.append((i + 1) + " ");
            } else if (countDeliteli == maxDeliteli && countDeliteli != 0) {
                result.append((i + 1) + " ");
            }
        }

        if (result.length() == 0){
            System.out.println("No winners");
        }else{
            System.out.println(maxDeliteli);
            System.out.println(result);
        }
    }

    private static int gcdThing(int a, int b) {
        BigInteger b1 = BigInteger.valueOf(a);
        BigInteger b2 = BigInteger.valueOf(b);
        BigInteger gcd = b1.gcd(b2);
        return gcd.intValue();
    }
}