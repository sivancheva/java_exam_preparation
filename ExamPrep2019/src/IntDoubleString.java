import java.util.Scanner;

public class IntDoubleString {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        String command = in.nextLine();
        String number = in.nextLine();

        switch (command){
            case "integer":
                System.out.println(Integer.parseInt(number) + 1);
                break;

            case "real":
                System.out.printf("%.2f", Double.parseDouble(number) + 1);
                break;

            case "text":
                System.out.println(number + "*");
                break;
        }
    }
}
