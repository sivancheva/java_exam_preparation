import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Interest {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        double amount = Double.parseDouble(reader.readLine());

        for (int i = 0; i < 3 ; i++) {
            Double interest = (amount*5)/100;
            amount += interest;
            System.out.printf("%.2f \n",amount);
        }
    }
}
