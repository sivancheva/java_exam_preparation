import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class IsAListSorted {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(reader.readLine());

        for (int i = 0; i < n; i++) {
            boolean isSoted = true;
            String[] intArray = reader.readLine().split(",");

            for (int j = 1; j < intArray.length; j++) {
                int currElement = Integer.parseInt(intArray[j]);
                int prevElement = Integer.parseInt(intArray[j-1]);
                if (currElement < prevElement) {
                    isSoted = false;
                    break;
                }
            }

            System.out.println(isSoted);
        }
    }
}