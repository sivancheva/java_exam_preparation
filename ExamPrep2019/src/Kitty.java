import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Kitty {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String[] symbols = reader.readLine().split("");
        String secondInput = reader.readLine();

        int[] path = Arrays.stream(secondInput.split(" "))
                .mapToInt(Integer::parseInt)
                .toArray();


        int souls = 0;
        int food = 0;
        int jumps = 0;
        int deadlocks = 0;
        boolean isDeadlocked = false;


        if (symbols[0].equals("@")) {
            souls++;
            symbols[0] = "";
        } else if (symbols[0].equals("*")) {
            food++;
            symbols[0]= "";
        } else if (symbols[0].equals("x")) {
            isDeadlocked = true;
            Print(jumps, souls, food, deadlocks, isDeadlocked);
            return;
        }

        int currentField = 0;
        for (int i = 0; i < path.length; i++) {
            jumps++;
            int fieldsToJump = path[i];
            int fieldToLand = ((currentField + fieldsToJump) % symbols.length + symbols.length) % symbols.length;

            if (symbols[fieldToLand].equals("@")) {
                souls++;
                symbols[fieldToLand] = "";
            } else if (symbols[fieldToLand].equals("*")) {
                food++;
                symbols[fieldToLand] = "";
            }  else if (symbols[fieldToLand].equals("x")) {
                deadlocks++;
                if (fieldToLand % 2 == 0 && souls > 0) {
                    souls--;
                    symbols[fieldToLand] = "@";
                } else if (fieldToLand % 2 != 0 && food > 0) {
                    food--;
                    symbols[fieldToLand] = "*";
                } else {
                    isDeadlocked = true;
                    break;
                }
            }
            currentField = fieldToLand;
            if (isDeadlocked) {
                break;
            }
        }

        Print(jumps, souls, food, deadlocks, isDeadlocked);
    }

    public static void Print(int jumps, int souls, int food, int deadlocks, boolean isDead) {
        if (isDead) {
            System.out.printf("You are deadlocked, you greedy kitty!\n" +
                    "Jumps before deadlock: %d", jumps);
        } else {
            System.out.printf("Coder souls collected: %d\n" +
                    "Food collected: %d\n" +
                    "Deadlocks: %d", souls, food, deadlocks);
        }
    }
}
