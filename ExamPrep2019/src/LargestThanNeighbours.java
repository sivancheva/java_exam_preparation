import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class LargestThanNeighbours {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(reader.readLine());
        int[] intArray = Arrays.stream(reader.readLine().split(" "))
                .mapToInt(Integer::parseInt)
                .toArray();

        int count = 0;

        if (n > 3){
            for (int i = 1; i < intArray.length -1 ; i++) {
                int currNum = intArray[i];
                int prevNum = intArray[i-1];
                int nextNum = intArray[i+1];

                if (currNum > nextNum && currNum > prevNum){
                    count++;
                }
            }
        }
        System.out.println(count);
    }
}