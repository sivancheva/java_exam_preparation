import java.io.BufferedReader;
import java.io.InputStreamReader;

public class LongSequence {
    public static void main(String[] args) throws Exception {

        for (int i = 2; i <= 1002; i++) {
            if (i % 2 != 0) {
                System.out.println(-1*i);
            }else{
                System.out.println(i);
            }
        }
    }
}
