import java.util.Scanner;

public class LongestIncreasingSequence {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        int n = in.nextInt();
        int[] myArray = new int[n];

        for (int i = 0; i < n ; i++) {
            myArray[i] = in.nextInt();
        }

        int maxLength = 1;
        int currLength = 1;

        for (int i = 0; i < n-1; i++) {

            if (myArray[i] < myArray[i+1]){
                currLength++;
            } else{
                currLength = 1;
            }
            if (currLength > maxLength){
                maxLength = currLength;
            }
        }

        System.out.print(maxLength);
    }
}
