import java.util.Scanner;

public class LongestSequenceOfEqual {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int maxLength = 1;
        int currLength = 1;

        int[] array = new int[n];

        for (int i = 0; i < n; i++) {
            array[i] = in.nextInt();
        }
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] == array[i + 1]) {
                currLength++;
            } else {
                currLength = 1;
            }
            if (currLength > maxLength){
                maxLength = currLength;
            }
        }
        System.out.print(maxLength);
    }
}
