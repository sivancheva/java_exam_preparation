import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Lucky {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String input = reader.readLine();

        int sumASCI = (int)(input.charAt(0) + input.charAt(1) +input.charAt(6) +input.charAt(7))/10 ;

        int multipDigits = 1;

        for (int i = 2; i < 6; i++) {
            multipDigits *= input.charAt(i) - '0';
        }

        if (sumASCI == multipDigits){
            System.out.printf("Yes %d", sumASCI);
        }else{
            System.out.println("No");
        }
    }
}