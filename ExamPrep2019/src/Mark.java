import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Mark {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(reader.readLine().trim());

        double[] intArray = Arrays.stream(reader.readLine().split(" "))
                .mapToDouble(Double::parseDouble)
                .sorted()
                .toArray();
        double result = 0;

        double minMark = intArray[0];
        double maxMark = intArray[n - 1];

        double sum = 0;
        int count = 0;

        if (minMark == maxMark) {
            result = minMark;
            System.out.printf("%d", (int) result);
            return;
        }

        for (int i = 1; i < n - 1; i++) {
            double currElement = intArray[i];
            sum += currElement;
            count++;
        }

        result = Math.round(sum  / (double) count);

        System.out.printf("%d",(int)result);
    }
}

