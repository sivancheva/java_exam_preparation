import java.util.Arrays;
import java.util.Scanner;

public class MaxSumOfKElements {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int k = in.nextInt();

        int maxSum = 0;
        int[] myIntArray = new int[n];

        for (int i = 0; i < n ; i++) {
            myIntArray[i] = in.nextInt();
        }
        Arrays.sort(myIntArray);

        for (int i = n-1; i > n-k-1 ; i--) {
            maxSum +=  myIntArray[i];
        }

        System.out.print(maxSum);
    }
}
