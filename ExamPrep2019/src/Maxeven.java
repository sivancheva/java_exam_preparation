import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Maxeven {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String[] inputArr = reader.readLine().split("[a-zA-Z\\s\\.]+");
        inputArr = Arrays.stream(inputArr).filter(s -> !s.isEmpty()).toArray(String[]::new);
        int[] ints = new int[inputArr.length];
        ints = Arrays.stream(inputArr).mapToInt(Integer::parseInt).toArray();
        Arrays.sort(ints);

        int result = -1;
        for (int i = ints.length - 1; i >= 0 ; i--) {
            if (ints[i] %2 == 0){
                result = ints[i];
                break;
            }
        }
        System.out.println(result);
    }
}
