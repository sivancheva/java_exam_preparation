import java.io.BufferedReader;
        import java.io.InputStreamReader;
        import java.util.Arrays;

public class MazeRunner {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(reader.readLine());

        StringBuilder result = new StringBuilder();

        for (int i = 0; i < n ; i++) {

            int sumEven = 0;
            int sumOdd = 0;

            int[] intArray = Arrays.stream(reader.readLine().split(""))
                    .mapToInt(Integer::parseInt)
                    .toArray();

            for (int j = 0; j < intArray.length; j++) {
                if (intArray[j] %2 == 0){
                    sumEven += intArray[j];
                }else{
                    sumOdd += intArray[j];
                }
            }

            if (sumEven > sumOdd){
                result.append("left" + "\n");
            }else if (sumEven < sumOdd){
                result.append("right" + "\n");
            }else{
                result.append("straight" + "\n");
            }
        }
        System.out.println(result);
    }
}