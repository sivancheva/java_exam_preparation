import java.io.BufferedReader;
import java.io.InputStreamReader;

public class MergingAndSquashing {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(reader.readLine());

        String[] numbers = new String[n];


        for (int i = 0; i < n ; i++) {
            numbers[i] = reader.readLine();
        }

        StringBuilder merged = Merging(numbers);
        StringBuilder squashed = Squasher(numbers);

        System.out.println(merged);
        System.out.println(squashed);
    }

    private static StringBuilder Merging(String[] numbers){
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < numbers.length-1 ; i++) {
            String currentNum = numbers[i];
            String nextNum = numbers[i+1];

            result.append(currentNum.charAt(1));
            result.append(nextNum.charAt(0));
            result.append(" ");
        }
        return  result;
    }

    private static StringBuilder Squasher(String[] numbers){
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < numbers.length-1 ; i++) {
            String currentNum = numbers[i];
            String nextNum = numbers[i + 1];

            result.append(currentNum.charAt(0));
            int firstDigit = currentNum.charAt(1) - '0';
            int secondDigit = nextNum.charAt(0) - '0';

            int sum = firstDigit + secondDigit;
            if (sum >= 10){
                result.append(Integer.toString(sum).charAt(1));
            }else{
                result.append(Integer.toString(sum).charAt(0));
            }
            result.append(nextNum.charAt(1));
            result.append(" ");
        }
        return result;
    }
}
