import java.util.Arrays;
import java.util.Scanner;

public class MinMaxSumAverage {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int n = in.nextInt();
        double[] numbers = new double[n];

        for (int i = 0; i < n; i++) {
            numbers[i] = in.nextDouble();
        }
        Arrays.sort(numbers);

        double min = numbers[0];
        double max = numbers[n-1];
        double sum =0;
        double average = 0;

        for (int i = 0; i < n; i++) {
            sum += numbers[i];
        }

        average = sum/n;

        System.out.printf("min=%.2f\n" +
                "max=%.2f\n" +
                "sum=%.2f\n" +
                "avg=%.2f", min, max, sum, average);

    }
}
