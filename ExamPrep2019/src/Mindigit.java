import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Mindigit {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int[] intArray = Arrays.stream(reader.readLine().split(""))
                .mapToInt(Integer::parseInt)
                .toArray();

        int minElement = Integer.MAX_VALUE;

        for (int i = 0; i < intArray.length ; i++) {
            int currElement = intArray[i];
            if ( currElement != 0 && currElement < minElement ){
                minElement = currElement;
            }
        }
        System.out.println(minElement);
    }
}
