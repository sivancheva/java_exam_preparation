import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class MissCat {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(reader.readLine());
        int[] cats = new int[10];
        int[] votes = new int[10];

        for (int i = 0; i < n; i++) {
            int vote = Integer.parseInt(reader.readLine());
            cats[vote-1]++;
            votes[vote-1]++;
        }

        Arrays.sort(votes);
        int maxVotes = votes[votes.length-1];

        int winnerNum = findIndex(cats, maxVotes);

        System.out.println(winnerNum +1);
    }

    public static int findIndex(int arr[], int t)
    {
        // if array is Null
        if (arr == null) {
            return -1;
        }

        // find length of array
        int len = arr.length;
        int i = 0;

        // traverse in the array
        while (i < len) {

            // if the i-th element is t
            // then return the index
            if (arr[i] == t) {
                return i;
            }
            else {
                i = i + 1;
            }
        }
        return -1;
    }
}