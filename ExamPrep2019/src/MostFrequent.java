import java.util.Arrays;
import java.util.Scanner;

public class MostFrequent {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int n = in.nextInt();
        int[] numbers = new int[n];
        for (int i = 0; i < n; i++) {
            numbers[i] = in.nextInt();
        }

        Arrays.sort(numbers);
        int maxLength = 1;
        int repeatedNum = 0;
        int currLength = 1;

        for (int i = 0; i < n-1; i++) {

            int currRepeatedNum = numbers[i];

             if (numbers[i] == numbers[i+1])   {
                 currLength++;

             }else{
                 currLength = 1;
             }
             if (currLength > maxLength){

                 maxLength = currLength;
                  repeatedNum = currRepeatedNum;
             }
        }
        System.out.printf("%d (%d times)" ,repeatedNum, maxLength);
    }
}
