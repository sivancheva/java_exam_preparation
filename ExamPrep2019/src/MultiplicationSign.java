import java.util.Scanner;

public class MultiplicationSign {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        Double firstNum = in.nextDouble();
        Double secondNum = in.nextDouble();
        Double thirdNum = in.nextDouble();

        if (firstNum == 0 || secondNum == 0 || thirdNum == 0){
            System.out.println(0);
            return;
        }
        if ((firstNum > 0 && secondNum > 0 && thirdNum > 0) ||
                (firstNum < 0 && secondNum < 0 && thirdNum > 0) ||
                (firstNum < 0 && secondNum > 0 && thirdNum < 0) ||
                (firstNum > 0 && secondNum < 0 && thirdNum < 0) ){
            System.out.println("+");
        }else{
            System.out.println("-");
        }
    }
}
