import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigInteger;

public class Mutating {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(reader.readLine().trim());

        String[] inputArray = reader.readLine().split(" ");

        int countMutated = 0;
        for (int i = 0; i < inputArray.length ; i++) {
            int initialNum = Integer.parseInt(inputArray[i]);
            String mutated = "";

            String currNum = inputArray[i];

            for (int j = 0; j < currNum.length(); j++) {
                int currDigit = currNum.charAt(j) - '0';
                if (currDigit == 0 ){
                    currDigit = 9;
                }else if(currDigit == 9){
                    currDigit = 0;
                }else if (currDigit% 2 == 0){
                    currDigit--;
                }else if(currDigit%2 != 0){
                    currDigit++;
                }
                mutated += currDigit;
            }
            int mutatedNum = Integer.parseInt(mutated);
            int gcd = gcdThing(initialNum, mutatedNum);
            if (gcd <= 9){
                countMutated++;
            }
        }

        System.out.println(countMutated);
    }

    private static int gcdThing(int a, int b) {
        BigInteger b1 = BigInteger.valueOf(a);
        BigInteger b2 = BigInteger.valueOf(b);
        BigInteger gcd = b1.gcd(b2);
        return gcd.intValue();
    }
}

