import java.io.BufferedReader;
import java.io.InputStreamReader;

//90/100 to4ki
public class NaPlaj {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String[] times = reader.readLine().split(" ");

        String[] timeLoni = new String[4];
        System.arraycopy(times, 0, timeLoni, 0, 4);

        String[] timeMoni = new String[4];
        System.arraycopy(times, 4, timeMoni, 0, 4);

        int startLoniA1 = Integer.parseInt(timeLoni[0] + timeLoni[1]);
        int endLoniL1 = Integer.parseInt(timeLoni[2] + timeLoni[3]);

        int startMoniA2 = Integer.parseInt(timeMoni[0] + timeMoni[1]);
        int endMoniL2= Integer.parseInt(timeMoni[2] + timeMoni[3]);

        StringBuilder result = new StringBuilder();

        if (startMoniA2 > endLoniL1){
            System.out.println("No");
            return;
        }else if (endMoniL2 < startLoniA1){
            System.out.println("No");
            return;
        }else if (startMoniA2 >= startLoniA1 && endMoniL2<=endLoniL1){
            for (int i = 0; i < 4; i++) {
                result.append(timeMoni[i] + " ");
            }
        }else if (startMoniA2 >= startLoniA1 && startMoniA2 <=endLoniL1 && endMoniL2>= endLoniL1){
            result.append(timeMoni[0] + " ");
            result.append(timeMoni[1] + " ");
            result.append(timeLoni[2] + " ");
            result.append(timeLoni[3] + " ");
        }else if (startMoniA2<= startLoniA1 && endMoniL2<= endLoniL1 && endMoniL2>= startLoniA1){
            result.append(timeLoni[0] + " ");
            result.append(timeLoni[1] + " ");
            result.append(timeMoni[2] + " ");
            result.append(timeMoni[3] + " ");
        }else if (startLoniA1>=startMoniA2 && endLoniL1<=endMoniL2){
            result.append(timeLoni[0] + " ");
            result.append(timeLoni[1] + " ");
            result.append(timeLoni[2] + " ");
            result.append(timeLoni[3] + " ");
        }
        System.out.println(result);
    }
}