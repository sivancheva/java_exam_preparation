import java.util.Scanner;

public class NotDivisableNumber {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        int n = in.nextInt();

        StringBuilder result = new StringBuilder();
        for (int i = 1; i <= n; i++) {
            if (i%3 != 0 && i%7 != 0){
                result.append(i + " ");
            }
        }
        System.out.println(result);
    }
}
