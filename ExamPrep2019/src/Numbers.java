import java.util.Scanner;

public class Numbers {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String strToModify = "";

        String input = in.nextLine();

        while (!input.equals("end")){

            String[] commands = input.split(" ");
            String command = commands[0];
            String digit = "";

            if (commands.length > 1){
                digit = commands[1];
            }
            switch (command){

                case "set":
                    strToModify = digit;
                    break;
                case "front-add":
                    strToModify = digit + strToModify;
                    break;
                case "front-remove":
                    if (strToModify.length()>0){
                        strToModify = strToModify.substring(1);
                    }
                    break;
                case "back-add":
                    strToModify = strToModify + digit;
                    break;
                case "back-remove":
                    if (strToModify.length()>0){
                        strToModify = strToModify.substring(0, strToModify.length() - 1);
                    }
                    break;
                case "reverse":
                    strToModify = new StringBuilder(strToModify).reverse().toString();
                    break;
                case "print":
                    System.out.println(strToModify);
                    break;
            }
            input = in.nextLine();
        }
    }
}
