import java.io.BufferedReader;
import java.io.InputStreamReader;

public class NumbersTriangle {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(reader.readLine());

        for (int i = 1; i <= n + 1; i++) {
            for (int j = 1; j < i; j++) {
                System.out.print(j + " ");
            }
            System.out.println();
        }

        for (int i = n; i >= 1; i--) {
            for (int j = 1; j < i ; j++) {
                System.out.print(j + " ");
            }
            System.out.println();
        }

        System.out.println();
    }
}