import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class OddNumbers {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(reader.readLine());

        Map<String, Integer> result = new HashMap<>();

        for (int i = 0; i < n; i++) {
            String num = reader.readLine();

            if (!result.containsKey(num)){
                result.put(num, 1);
            }else{
                result.put(num, result.get(num) +1);
            }
        }

        for (String key : result.keySet()) {
            int value = result.get(key);
            if (value%2 != 0){
                System.out.println(key);
                return;
            }
        }
    }
}