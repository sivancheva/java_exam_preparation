import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Arrays;
import java.io.InputStreamReader;

public class OddOrEvenProduct {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String inputN = reader.readLine();

        int[] intArray = Arrays.stream(reader.readLine().split(" "))
                .mapToInt(Integer::parseInt)
                .toArray();

        BigInteger oddProduct = new BigInteger("1");
        BigInteger evenProduct = new BigInteger("1");

        for (int i = 0; i < intArray.length; i++) {
            if (i % 2 == 0) {
                oddProduct = oddProduct.multiply(BigInteger.valueOf(intArray[i]));
            } else {
                evenProduct = evenProduct.multiply(BigInteger.valueOf(intArray[i]));
            }
        }

        if (evenProduct.equals(oddProduct)){
            System.out.printf("yes %d", evenProduct);
        }else{
            System.out.printf("no %d %d", oddProduct, evenProduct);
        }
    }
}
