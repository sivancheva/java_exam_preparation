import java.io.BufferedReader;
import java.io.InputStreamReader;

public class PaperCutter {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int searchedSheetsOfPaper = Integer.parseInt(reader.readLine());

        int[] paperSizes = new int[11];
        int paperSizeComparedToA10 = 1024;
        int sum = 0;

        for (int i = 0; i < 11; i++) {

            paperSizes[i] = paperSizeComparedToA10;
            paperSizeComparedToA10 /= 2;
        }

        for (int i = 0; i < 11; i++) {

            boolean solved = false;
            if ((double)paperSizes[i] / (double) searchedSheetsOfPaper == 1) {
                paperSizes[i] = 0;
                solved = true;
                break;
            }
            if ((double)paperSizes[i] / (double) searchedSheetsOfPaper < 1) {
                sum += paperSizes[i];
                paperSizes[i] = 0;

                for (int j = i+1; j < 11; j++) {

                    if (sum + paperSizes[j] == searchedSheetsOfPaper) {
                        sum += paperSizes[j];
                        paperSizes[j] = 0;
                        solved = true;
                        break;
                    } else if (sum + paperSizes[j] < searchedSheetsOfPaper) {
                        sum += paperSizes[j];
                        paperSizes[j] = 0;
                    }
                }
            }
            if (solved) {
                break;
            }
        }

        StringBuilder result = new StringBuilder();

        for (int i = 0; i < 11; i++) {
            if (paperSizes[i] != 0){
                result.append("A"+ i +"\n");
            }
        }
        System.out.println(result);
    }
}