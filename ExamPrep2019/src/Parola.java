import java.util.Scanner;

public class Parola {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        String input = in.nextLine();
        String[] parts = input.split(" ");
        char[] numAsString = parts[0].toCharArray();
        
       
        String reversedNum = "";
        int k = Integer.parseInt(parts[1]);

        for (int i = 5; i > 2  ; i--) {
            reversedNum += numAsString[i];
        }
        for (int i = 2; i >= 0 ; i--) {
            reversedNum += numAsString[i];
        }

        int reversedNumberAsInt = Integer.parseInt(reversedNum);

        int holePart = reversedNumberAsInt/k;
        int chastno = reversedNumberAsInt%k;
        int result = holePart + chastno;

        System.out.println(result);
    }
}
