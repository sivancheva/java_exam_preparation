import java.io.BufferedReader;
import java.io.InputStreamReader;

public class PhoneBill {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        double smsCount = Double.parseDouble(reader.readLine());
        double minutes = Double.parseDouble(reader.readLine());

        double totalSum = 12;
        final double priceSMS = 0.06;
        final double pricePhoneCalls = 0.10;

        double additionalSMS = 0;
        double taksaAdditionalSMS = 0;

        double additionalPhones = 0;
        double taksaAddCalls = 0;

        if (smsCount > 20){
            additionalSMS = smsCount - 20;
            taksaAdditionalSMS = additionalSMS*priceSMS;
        }
        if (minutes > 60){
            additionalPhones = minutes - 60;
            taksaAddCalls = additionalPhones*pricePhoneCalls;
        }

        double taxes = (taksaAddCalls + taksaAdditionalSMS)*20/100;

        totalSum += taksaAddCalls + taksaAdditionalSMS + taxes;

        System.out.printf("%.0f additional messages for %.2f levas \n",additionalSMS, taksaAdditionalSMS );
        System.out.printf("%.0f additional minutes for %.2f levas \n",additionalPhones, taksaAddCalls );
        System.out.printf("%.2f additional taxes \n",taxes );
        System.out.printf("%.2f total bill \n",totalSum );
    }
}
