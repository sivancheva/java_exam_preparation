import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.stream.IntStream;

public class Pier {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(reader.readLine().trim());
        int[] intArray = Arrays.stream(reader.readLine().split(" "))
                .mapToInt(Integer::parseInt)
                .toArray();

        int difference = Integer.MAX_VALUE;

        if (n == 1){
            difference = 1;
        }
        if (n == 2){
            difference = Math.abs(intArray[0] - intArray[1]);
        }

        for (int i = 0; i < intArray.length -1; i++) {
            int innerFirstHalfSum = 0;
            int innerSecondHalfSum = 0;

            int[] firstSubArray = new int[i+1];
            System.arraycopy( intArray, 0, firstSubArray, 0, i+1 );
            innerFirstHalfSum = IntStream.of(firstSubArray).sum();

            int[] secondSubArray = new int[n-(i+1)];
            System.arraycopy( intArray, (i+1), secondSubArray, 0, intArray.length - (i+1) );
            innerSecondHalfSum = IntStream.of(secondSubArray).sum();


            int currDufference = Math.abs(innerFirstHalfSum - innerSecondHalfSum);
            if (currDufference < difference){
                difference = currDufference;
            }
        }
        System.out.println(difference);
    }
}
