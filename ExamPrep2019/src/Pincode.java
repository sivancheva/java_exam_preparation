import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Pincode {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String[] input = reader.readLine().split(" ");

        int firstNum = Integer.parseInt(new StringBuilder(input[0]).reverse().toString());
        int secondNum = Integer.parseInt(new StringBuilder(input[1]).reverse().toString());

        System.out.println(Math.max(firstNum, secondNum));
    }
}
