import java.io.BufferedReader;
        import java.io.InputStreamReader;
        import java.util.Arrays;

public class PrimeTriangle {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(reader.readLine());
        int countPrimes = 0;

        for (int i = 1; i <= n ; i++) {
            if (IsPrime(i)){
                countPrimes++;
            }
        }

        StringBuilder result = new StringBuilder();

        for (int i = 1; i <= countPrimes ; i++) {
            int currCountPrimes = i;
            int start = 1;
            while (currCountPrimes > 0){
                 if (IsPrime(start)){
                    result.append("1");
                    currCountPrimes--;
                }else{
                    result.append("0");
                }
                start++;
            }
            result.append("\n");
        }
        System.out.println(result);
    }

    private static boolean IsPrime (int num){
        boolean isPrime = true;
        for(int divisor = 2; divisor <= num / 2; divisor++) {
            if (num % divisor == 0) {
                isPrime = false;
                break; // num is not a prime, no reason to continue checking
            }
        }
        return isPrime;
    }
}