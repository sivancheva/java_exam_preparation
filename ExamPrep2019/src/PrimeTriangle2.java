import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class PrimeTriangle2 {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(reader.readLine());

        List<Integer> primeNums = new ArrayList<>();

        for (int i = 1; i <= n; i++) {
            if (IsPrime(i)){
                primeNums.add(i);
            }
        }

        for (int i = 0; i < primeNums.size(); i++) {
            StringBuilder result = new StringBuilder();

            for (int j = 1; j <= primeNums.get(i); j++) {

                if (primeNums.contains(j)){
                    result.append("1");
                }else{
                    result.append("0");
                }
            }
            System.out.println(result);
        }
    }

    private static boolean IsPrime (int num){
        boolean isPrime = true;
        for(int divisor = 2; divisor <= num / 2; divisor++) {
            if (num % divisor == 0) {
                isPrime = false;
                break; // num is not a prime, no reason to continue checking
            }
        }
        return isPrime;
    }
}
