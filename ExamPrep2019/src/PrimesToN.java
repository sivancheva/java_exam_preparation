import java.io.BufferedReader;
import java.io.InputStreamReader;

public class PrimesToN {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(reader.readLine());

        for (int i = 1; i <= n ; i++) {
            if (IsPrime(i)){
                System.out.printf("%d ", i);
            }
        }

        System.out.println();
    }

    private static boolean IsPrime(int num) {
        boolean isPrime = true;
        for (int divisor = 2; divisor <= num / 2; divisor++) {
            if (num % divisor == 0) {
                isPrime = false;
                break; // num is not a prime, no reason to continue checking
            }
        }
        return isPrime;
    }
}