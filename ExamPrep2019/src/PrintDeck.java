import java.util.Scanner;

public class PrintDeck {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        String sign = in.nextLine();
        String[] cards = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"};

        int index = -1;

        for (int i=0;i<cards.length;i++) {
            if (cards[i].equals(sign)) {
                index = i;
                break;
            }
        }

        for (int i = 0; i <= index ; i++) {
            System.out.printf("%s of spades, %s of clubs, %s of hearts, %s of diamonds\n", cards[i], cards[i], cards[i], cards[i]);
        }
    }
}
