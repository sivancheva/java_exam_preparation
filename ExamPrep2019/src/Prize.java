import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Prize {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int[] ocenki = Arrays.stream(reader.readLine().split(" "))
                .mapToInt(Integer::parseInt)
                .toArray();
        Arrays.sort(ocenki);

        boolean has6 = Contains(6, ocenki);

        if (ocenki[0] == 2 || !has6){
            System.out.println("No");
            return;
        }
        StringBuilder result = new StringBuilder();
        for (int i = ocenki.length -1 ; i >= 0 ; i--) {
            if (ocenki[i] != 6){
                break;
            }else {
                result.append("*");
            }
        }

        System.out.println(result);
    }

    public static boolean Contains(int n, int[] array) {
        for (int x : array) {
            if (x ==  n) {
                return true;
            }
        }
        return false;
    }

}
