import java.io.BufferedReader;//Импортва се най-отгоре.
import java.io.InputStreamReader;//Импортва се най-отгоре
import java.util.Arrays;

public class Rankove {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String inputFirst = reader.readLine();//Чете стринг от конзолата
        int n = Integer.parseInt(inputFirst);
        String numbers = reader.readLine();

        int[] originalArr = Arrays.stream(numbers.split(" "))
                .mapToInt(Integer::parseInt)
                .toArray();

        int[] copyOfArray = originalArr.clone();
        Arrays.sort(copyOfArray);
        int[] reversedArr = new int[n];

        int j = 0;
        for (int i = n-1; i >=0  ; i--) {
            reversedArr[j] = copyOfArray[i];
            j++;
        }

        StringBuilder result = new StringBuilder();

        for (int i = 0; i < n ; i++) {
            int num = originalArr[i];
            int index = findIndex(reversedArr, num);

            result.append(index+1);
        }
        System.out.println(result);
    }

    public static int findIndex(int arr[], int t)
    {
        // if array is Null
        if (arr == null) {
            return -1;
        }

        // find length of array
        int len = arr.length;
        int i = 0;

        // traverse in the array
        while (i < len) {

            // if the i-th element is t
            // then return the index
            if (arr[i] == t) {
                return i;
            }
            else {
                i = i + 1;
            }
        }
        return -1;
    }
}
