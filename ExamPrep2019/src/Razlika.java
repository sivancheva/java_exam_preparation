import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Razlika {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int[] intArray = Arrays.stream(reader.readLine().split(" "))
                .mapToInt(Integer::parseInt)
                .toArray();

        int result = Math.abs(intArray[0] - intArray[1]);
        System.out.println(result);
    }
}