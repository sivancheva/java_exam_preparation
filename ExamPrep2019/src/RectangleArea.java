import java.io.BufferedReader;
import java.io.InputStreamReader;

public class RectangleArea {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int width = Integer.parseInt(reader.readLine());
        int height = Integer.parseInt(reader.readLine());

        System.out.println(width*height);
    }
}