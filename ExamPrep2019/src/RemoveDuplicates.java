import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;

public class RemoveDuplicates {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        List<String> myList = new ArrayList<String>(Arrays.asList(reader.readLine().split(",")));

        List<String> result = new ArrayList<>();

        for (int i = 0; i < myList.size() ; i++) {
            String s = myList.get(i);
            if (!result.contains(s)){
                result.add(s);
            }
        }
        System.out.println(String.join(",", result));
    }
}
