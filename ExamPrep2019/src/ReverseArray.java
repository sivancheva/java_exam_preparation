import java.io.BufferedReader;
import java.io.InputStreamReader;


public class ReverseArray {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String[] input = reader.readLine().split(" ");

        StringBuilder result = new StringBuilder();

        for (int i = input.length - 1; i > 0; i--) {
            result.append(input[i] + ", ");
        }
        result.append(input[0]);

        System.out.println(result);
    }
}