import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ReverseNumber {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String number = reader.readLine();

        StringBuilder result = new StringBuilder();

        for (int i = number.length() -1  ; i >= 0 ; i--) {
            result.append(number.charAt(i));
        }
        System.out.println(result);
    }
}