import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Ring {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String[] input = reader.readLine().split(" ");

        int arrLength = Integer.parseInt(input[0]);

        int[] u4enici = new int[arrLength];

        for (int i = 0; i < u4enici.length; i++) {
            u4enici[i] = Integer.parseInt(reader.readLine());
        }

        int countJumps = arrLength;
        int currentField = 0;
        int jumpLength = Integer.parseInt(input[1])-1;

        while (countJumps > 1){

            int fieldToLand = (currentField + jumpLength) % u4enici.length;

            if (u4enici[fieldToLand] == -1){
                jumpLength++;
                continue;
            }
            currentField = fieldToLand;
            jumpLength = u4enici[fieldToLand];
            u4enici[fieldToLand] = -1;
            countJumps--;
        }

        for (int i = 0; i < u4enici.length ; i++) {
            if (u4enici[i] != -1){
                System.out.println(i+1);
            }
        }
    }
}