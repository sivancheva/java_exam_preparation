import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RotateList {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        List<String> myList = new ArrayList<String>(Arrays.asList(reader.readLine().split(",")));

        int n = Integer.parseInt(reader.readLine());

        for (int i = 0; i < n ; i++) {
            int listSize = myList.size();
            String s = myList.get(0);
            myList.remove(0);
            myList.add(s);
        }

            System.out.println(String.join(",", myList));

    }
}
