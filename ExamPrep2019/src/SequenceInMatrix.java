import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class SequenceInMatrix {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int[] intArray = Arrays.stream(reader.readLine().split(" "))
                .mapToInt(Integer::parseInt)
                .toArray();

        int nRows = intArray[0];
        int mCols = intArray[1];

        String[][] matrx = new String[nRows][];
        for (int i = 0; i < nRows; i++) {
            String[] row = reader.readLine().split(" ");
            matrx[i] = row;
        }

        int maxLength = 1;
        for (int i = 0; i < nRows; i++) {
            int currMaxLen = 1;
            String currElement = matrx[i][0];
            String nextElement = "";
            for (int j = 0; j < mCols - 1; j++) {

                nextElement = matrx[i][j + 1];

                if (currElement.equals(nextElement)) {
                    currMaxLen++;
                } else{
                    currElement = matrx[i][j+1];
                    if (currMaxLen > maxLength){
                        maxLength = currMaxLen;
                    }
                    currMaxLen = 1;
                }
            }
            if (currMaxLen > maxLength){
                maxLength =  currMaxLen;
            }
        }

        for (int i = 0; i < mCols; i++) {
            int currMaxLen = 1;
            String currElement = matrx[0][i];
            String nextElement = "";
            for (int j = 0; j < nRows - 1; j++) {

                nextElement = matrx[j+1][i];

                if (currElement.equals(nextElement)) {
                    currMaxLen++;
                } else{
                    currElement = matrx[j+1][i];
                    if (currMaxLen > maxLength){
                        maxLength = currMaxLen;
                    }
                    currMaxLen = 1;
                }
            }
            if (currMaxLen > maxLength){
                maxLength =  currMaxLen;
            }
        }
        System.out.println(maxLength);
    }
}
