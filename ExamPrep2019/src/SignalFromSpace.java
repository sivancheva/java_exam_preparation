
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class SignalFromSpace {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String input = reader.readLine();
        StringBuilder result = new StringBuilder();
        result.append(input.charAt(0));

        for (int i = 0; i < input.length()-1; i++) {
            Character currChar = input.charAt(i);
            Character nextChar = input.charAt(i+1);

            if (!currChar.equals(nextChar)){
                result.append(nextChar);
            }
        }

        System.out.println(result);
    }
}