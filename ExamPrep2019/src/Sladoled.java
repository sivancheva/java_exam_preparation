import java.io.BufferedReader;
import java.io.InputStreamReader;

//80/100

public class Sladoled {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String[] input = reader.readLine().split(" ");
        int broiSlon4eta = Integer.parseInt(input[0]);
        String sladolediZaJadene = input[1];
        int count = 0;

        if (broiSlon4eta <= sladolediZaJadene.length()){
            for (int i = 0; i < broiSlon4eta ; i++) {
                if (sladolediZaJadene.charAt(i) - '0' == 0){
                    count++;
                }
            }
        }else{
            count = broiSlon4eta - sladolediZaJadene.length();
            for (int i = 0; i < sladolediZaJadene.length() ; i++) {
                if (sladolediZaJadene.charAt(i) - '0' == 0){
                    count++;
                }
            }
        }

        System.out.println(count);
    }
}