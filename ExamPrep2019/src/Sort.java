import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Sort {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int[] flowers = Arrays.stream(reader.readLine().split(" "))
                .mapToInt(Integer::parseInt)
                .toArray();


            if (flowers[0] < flowers[1] && flowers[1] < flowers[2] && flowers[2] < flowers[3]){
                System.out.println("Ascending");
            }else if(flowers[0] > flowers[1] && flowers[1] > flowers[2] && flowers[2] > flowers[3]){
                System.out.println("Descending");
            }else{
                System.out.println("Mixed");
            }

        System.out.println();
    }
}
