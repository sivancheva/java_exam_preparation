import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SortNumbers {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

       String[] intArray = reader.readLine().split(", ");

        List<Integer> list = new ArrayList<Integer>();

        for (String i : intArray)
        {
            list.add(Integer.parseInt(i));
        }
        Collections.sort(list);
        Collections.reverse(list);

        StringBuilder result = new StringBuilder();
        for (int i : list){
           result.append(i + ", ");
        }
        result.deleteCharAt(result.lastIndexOf(", "));
        System.out.println(result);

    }
}