import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class SortThreeNums {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        List<Integer> intArray = new ArrayList<>();
        intArray.add(Integer.parseInt(reader.readLine()));

        for (int i = 0; i < 2; i++) {
            int nextInt = Integer.parseInt(reader.readLine());
            if (nextInt > intArray.get(0)) {
                intArray.add(0, nextInt);
            } else {
                if (intArray.size() == 2 && nextInt > intArray.get(1)) {
                    intArray.add(1, nextInt);
                } else {
                    intArray.add(nextInt);
                }
            }
        }
        for (int num : intArray) {
            System.out.printf("%d ", num);
        }
    }
}