import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Speeds {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(reader.readLine());
        int maxSum = 0;
        int maxLengthOfGroup = 1;


        int[] speed = new int[n];

        for (int i = 0; i < n; i++) {
           speed[i] = Integer.parseInt(reader.readLine());
        }

        int currMaxSpeed = speed[0];
        int currSum = currMaxSpeed;
        int groupLength = 1;
        for (int i = 1; i < n ; i++) {
            if (speed[i] > currMaxSpeed){
                currSum += speed[i];
                groupLength++;
            }else{
                if (groupLength > maxLengthOfGroup){
                    maxLengthOfGroup = groupLength;
                    maxSum = currSum;
                }else if (groupLength == maxLengthOfGroup){
                    maxSum = Math.max(maxSum, currSum);
                }else{
                    currMaxSpeed = speed[i];
                    groupLength = 1;
                    currSum = 0;
                }
            }
        }
        System.out.println();
    }
}