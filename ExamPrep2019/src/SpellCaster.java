import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SpellCaster {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        StringBuilder modifiedWord = new StringBuilder();
        List<String> list = new ArrayList<String>(Arrays.asList(reader.readLine().split(" ")));
        String smallLetters = " abcdefghijklmnopqrstuvwxyz";

        while (list.size() != 0) {

            for (int i = 0; i < list.size(); i++) {
                String word = list.get(i);
                if (word.equals("")){
                    list.remove(i);
                    continue;
                }
                Character toAdd = word.charAt(word.length() - 1);
                modifiedWord.append(toAdd);
                word = word.substring(0, word.length() - 1);
                list.set(i, word);
            }
        }

        StringBuilder result = new StringBuilder(modifiedWord);

        for (int i = 0; i < modifiedWord.length(); i++) {
            char currentChar = result.charAt(i);
            result.deleteCharAt(i);
            int jumpLength = smallLetters.indexOf(Character.toLowerCase(currentChar));
            int fieldToLand = (i + jumpLength) % modifiedWord.length();
            result.insert(fieldToLand,currentChar);
        }
        System.out.println(result);
    }
}