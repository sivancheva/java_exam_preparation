import java.io.BufferedReader;
import java.io.InputStreamReader;

public class SquareRoot {
    public static void main(String[] args) throws Exception {

        Double n = 1234560.;

        Double result = Math.pow(n,0.5 );

        System.out.printf("%.0f", result);
    }
}