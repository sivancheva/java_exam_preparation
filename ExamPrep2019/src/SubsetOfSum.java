import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

//15/20
public class SubsetOfSum {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(reader.readLine());
        int[] intArray = Arrays.stream(reader.readLine().split(" "))
                .mapToInt(Integer::parseInt)
                .toArray();


        boolean thereIsASubsequesnce = false;
        for (int i = 0; i < intArray.length; i++) {

            int sum = 0;

            for (int j = i; j < intArray.length; j++) {

                sum += intArray[j];
                if (sum == n){
                   thereIsASubsequesnce = true;
                    break;
                }else if (sum > n){
                    break;
                }
            }
            if (thereIsASubsequesnce){
                break;
            }
        }

        if (thereIsASubsequesnce){
            System.out.println("yes");
        }else{
            System.out.println("no");
        }
    }
}