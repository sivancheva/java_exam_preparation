import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class SubsetOfSumVariant2 {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int sum = Integer.parseInt(reader.readLine());
        int[] intArray = Arrays.stream(reader.readLine().split(" "))
                .mapToInt(Integer::parseInt)
                .toArray();
        
        if (isSubsetSum(intArray, intArray.length, sum)){
            System.out.println("yes");
        }else{
            System.out.println("no");
        }
    }


    private static boolean isSubsetSum(int set[],
                               int n, int sum)
    {
        // Base Cases
        if (sum == 0)
            return true;
        if (n == 0 && sum != 0)
            return false;

        // If last element is greater than
        // sum, then ignore it
        if (set[n-1] > sum)
            return isSubsetSum(set, n-1, sum);

        /* else, check if sum can be obtained
        by any of the following
            (a) including the last element
            (b) excluding the last element */
        return isSubsetSum(set, n-1, sum) ||
                isSubsetSum(set, n-1, sum-set[n-1]);
    }
}

