import java.io.BufferedReader;
import java.io.InputStreamReader;


public class SumDigits {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String input = reader.readLine();

        int sum = 0;
        for (int i = 0; i < 4 ; i++) {
            sum += input.charAt(i) - '0';
        }
        System.out.println(sum);

    }
}