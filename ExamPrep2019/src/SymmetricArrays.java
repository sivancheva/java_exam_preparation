import java.io.BufferedReader;
import java.io.InputStreamReader;

public class SymmetricArrays {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(reader.readLine());


        for (int i = 0; i < n ; i++) {
            boolean isSymmetric = true;
            String[] nums = reader.readLine().split(" ");
            int numLength = nums.length;

            for (int j = 0; j < nums.length/2; j++) {
                String first = nums[j];
                String second = nums[numLength-j-1];

                if (!first.equals(second)){
                    isSymmetric = false;
                    break;
                }
            }
            if (isSymmetric){
                System.out.println("Yes");
            }else {
                System.out.println("No");
            }
        }
    }
}