import java.io.BufferedReader;
import java.io.InputStreamReader;

public class TainoSuob6tenie {
    public static void main(String[] args) throws Exception{

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String input = reader.readLine();

        StringBuilder result = new StringBuilder();

        for (int i = input.length() - 1; i >= 0 ; i--) {

            char currentstr = input.charAt(i);

            if (!Character.isDigit(currentstr)){
                result.append(currentstr);
            }
        }
        System.out.println(result);
    }
}
