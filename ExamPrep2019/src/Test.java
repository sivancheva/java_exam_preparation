import java.util.Scanner;

public class Test {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        int arraySize = in.nextInt();

        int[] numbers = new int[arraySize];

        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = 5 * i;
            System.out.println(numbers[i]);
        }
    }
}