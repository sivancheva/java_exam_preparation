import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class ThreeGroups {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        StringBuilder zeroReminderGroup = new StringBuilder();
        StringBuilder oneReminderGroup = new StringBuilder();
        StringBuilder twoReminderGroup = new StringBuilder();

        int[] intArray = Arrays.stream(reader.readLine().split(" "))
                .mapToInt(Integer::parseInt)
                .toArray();

        for (int i = 0; i < intArray.length; i++) {
            int currElement = intArray[i];

            if ( currElement % 3 == 0){
                zeroReminderGroup.append(currElement + " ");
            }else if (currElement%3 == 1){
                oneReminderGroup.append(currElement + " ");
            }else if (currElement% 3 == 2 ){
                twoReminderGroup.append(currElement+ " ");
            }
        }
            System.out.println(zeroReminderGroup);
            System.out.println(oneReminderGroup);
            System.out.println(twoReminderGroup);

    }
}
