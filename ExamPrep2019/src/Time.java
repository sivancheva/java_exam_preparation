import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Time {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int days = Integer.parseInt(reader.readLine());
        int hours = Integer.parseInt(reader.readLine());
        int minutes = Integer.parseInt(reader.readLine());
        int seconds = Integer.parseInt(reader.readLine());

        seconds += minutes*60;
        seconds += hours*3600;
        seconds += days*86400;

        System.out.println(seconds);
    }
}