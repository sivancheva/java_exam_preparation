import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Tips {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

       double price = Double.parseDouble(reader.readLine());

       double tip = price*10/100;

        System.out.printf("%.0f", price + tip);
    }
}
