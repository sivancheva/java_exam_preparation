import java.io.BufferedReader;
import java.io.InputStreamReader;

public class TrailingZeroesInFactorial {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(reader.readLine());

        int count = findTrailingZeros(n);
        System.out.println(count);
    }

    private static int findTrailingZeros(int n)
    {
        // Initialize result
        int count = 0;

        // Keep dividing n by powers of
        // 5 and update count
        for (int i = 5; n / i >= 1; i *= 5)
            count += n / i;

        return count;
    }
}