import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Trapezoid {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(reader.readLine());
        String dot = ".";
        String star = "*";

        StringBuilder result = new StringBuilder();

        String firstLineDotsToAppend =  new String(new char[n]).replace("\0", dot);
        String firstLineStarsToAppend =  new String(new char[n]).replace("\0", star);
        result.append(firstLineDotsToAppend).append(firstLineStarsToAppend).append("\n");

        for (int i = 0; i < n-1; i++) {
            StringBuilder toAppend = new StringBuilder();
            for (int j = 0; j < 2*n -1; j++) {
                toAppend.append(dot);
            }
            toAppend.append("*");
            toAppend.setCharAt(n-1-i, '*');
            toAppend.append("\n");
            result.append(toAppend);
        }
        String lastLineStarsToAppend =  new String(new char[2*n]).replace("\0", star);
        result.append(lastLineStarsToAppend);
        System.out.println(result);
    }
}
