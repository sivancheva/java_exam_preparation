import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class Tribonacci {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        List<BigInteger> list = new ArrayList<>();

        for (int i = 0; i < 3; i++) {
            long n = Long.parseLong(reader.readLine());
            BigInteger bi = BigInteger.valueOf(n);
            list.add(bi);
        }
        int searchedNum = Integer.parseInt(reader.readLine());

        int counter = 0;
        while (list.size() < searchedNum){

            BigInteger sum = new BigInteger("0");

            for (int i = counter; i < counter+3 ; i++) {
                BigInteger toAdd = list.get(i);
                sum = sum.add(toAdd);
            }
            list.add(sum);
            counter++;
        }
        System.out.println(list.get(list.size()-1));
    }
}