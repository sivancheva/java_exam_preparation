import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Vowel {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String input = reader.readLine();
        char[] vowels = {'a','e','i','o','u','y'};
        int countVowels = 0;
        int others = 0;

        for (int i = 0; i < input.length(); i++) {
            char currChar = input.charAt(i);
            boolean containsVowel = Contains(currChar, vowels);
            if (containsVowel){
                countVowels++;
            }else{
                others++;
            }
        }
        if (countVowels == others){
            System.out.println("Yes");
        }else{
            System.out.print(countVowels +" " + others);
        }

    }
    public static boolean Contains(char c, char[] array) {
        for (char x : array) {
            if (x == c) {
                return true;
            }
        }
        return false;
    }
}
