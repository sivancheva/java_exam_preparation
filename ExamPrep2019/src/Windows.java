import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Windows {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String[] input = reader.readLine().split(" ");


        int broiStukla = Integer.parseInt(input[0]);
        double price = Double.parseDouble(input[1]);
        double sumWindows = 0;

        for (int i = 0; i < broiStukla; i++) {
            double[] windowParams = Arrays.stream(reader.readLine().split(" "))
                    .mapToDouble(Double::parseDouble)
                    .toArray();
            if (windowParams[2] == 1){
                double width = windowParams[0];
                double heigh = windowParams[1];
                sumWindows += width*heigh;
            }
        }
        System.out.println((int)(sumWindows*price));
    }
}